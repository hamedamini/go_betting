package models

import "sync"

type Bettor struct {
	bettorAccountId int
	bettorPool      float32
}

type BettorType struct {
	sync.Mutex
	list map[int]*Bettor
}

var Bettors BettorType = BettorType{
	list: make(map[int]*Bettor),
}

//Create Bettor
func NewBettor(bettorAccountId int, bettorPool float32) *Bettor {
	return &Bettor{
		bettorAccountId: bettorAccountId,
		bettorPool:      bettorPool,
	}
}

//Create Bettor And add To All Bettors
func (bettor *BettorType) NewBettor() int {
	Bettor := NewBettor(len(bettor.list)+1, 1000.00)
	bettor.Lock()
	bettor.list[Bettor.bettorAccountId] = Bettor
	Liquiditypool.pool -= 1000.00
	bettor.Unlock()
	return Bettor.bettorAccountId
}

// Get Balance Of Bettor Pool
func (B *BettorType) GetBalance(bettorAccountId int) (isAccountExist bool, balance float32) {
	for _, v := range B.list {
		if v.bettorAccountId == bettorAccountId {
			return true, B.list[v.bettorAccountId].bettorPool
		}
	}
	return false, 0
}

// Add Amount Bet Value To  Bettor Poll
func addToBettorPool(bettorAccountId int, valuebetAmount float32) bool {
	for _, v := range Bettors.list {
		if v.bettorAccountId == bettorAccountId {
			Bettors.list[v.bettorAccountId].bettorPool += valuebetAmount
			return true
		}
	}
	return false
}

// Get Amount Bet Value To  Bettor Poll
func GetFromBettorPool(bettorAccountId int, valuebetAmount float32) bool {
	for _, v := range Bettors.list {
		if v.bettorAccountId == bettorAccountId {
			Bettors.list[v.bettorAccountId].bettorPool -= valuebetAmount
			return true
		}
	}
	return false
}
