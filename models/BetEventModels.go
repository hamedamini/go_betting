package models

import (
	"go_betting/customerror"
	"sync"
)

type BetEvent struct {
	betEventID int
	oddValue0  float32
	oddValue1  float32
	sumValue0  float32 // sum of pool of 0
	sumValue1  float32 // sum of pool of 1
}

type BetEventsType struct {
	sync.Mutex
	list map[int]*BetEvent
}

var BetEvents BetEventsType = BetEventsType{
	list: make(map[int]*BetEvent),
}

func NewBetEvent(betEventID int, oddValue0 float32, oddValue1 float32) *BetEvent {
	return &BetEvent{
		betEventID: betEventID,
		oddValue0:  oddValue0,
		oddValue1:  oddValue1,
		sumValue0:  0,
		sumValue1:  0,
	}
}

func (bts *BetEventsType) NewBetEvent(oddValue0 float32, oddValue1 float32) int {
	// +1 Because Auto Increment betEventID
	betEvent := NewBetEvent(len(bts.list)+1, oddValue0, oddValue1)
	bts.Lock()
	// if exist return error
	bts.list[betEvent.betEventID] = betEvent
	bts.Unlock()
	return betEvent.betEventID
}

//Transfer the payout amount from the liquidity pool to all the bettor’s account who won the bet
func handleResult(betEventID int, betvalue byte) error {
	//get all bet ids from bets list and send value losted bet to pool
	for betListId, betValue := range Bets.list {
		if betValue.bettingeventid == betEventID {
			if betvalue == 0 {
				Liquiditypool.pool += Bets.list[betListId].value1
				GetFromBettorPool(betValue.bettorAccountId, Bets.list[betListId].value1)
				Bets.list[betListId].value1 = 0
			} else if betvalue == 1 {
				Liquiditypool.pool += Bets.list[betListId].value0
				GetFromBettorPool(betValue.bettorAccountId, Bets.list[betListId].value0)
				Bets.list[betListId].value0 = 0
			} else {
				return customerror.ErrorHandleResult
			}
		}
	}
	//get all bet ids from bets list and send value wined bet to bettors and reduce from pool
	for betListId, betValue := range Bets.list {
		if betValue.bettingeventid == betEventID {
			if betvalue == 0 {
				Liquiditypool.pool -= Bets.list[betListId].value0
				addToBettorPool(betValue.bettorAccountId, Bets.list[betListId].value0)
				Bets.list[betListId].value0 = 0
			} else if betvalue == 1 {
				Liquiditypool.pool -= Bets.list[betListId].value1
				addToBettorPool(betValue.bettorAccountId, Bets.list[betListId].value0)
				Bets.list[betListId].value1 = 0
			} else {
				return customerror.ErrorHandleResult
			}
		}

	}
	return nil
}

//Transfer the payout amount from the liquidity pool to all the bettor’s account who won the bet
func CalculateBetResult(betEventID int, betvalue byte) error {
	if betvalue != 0 && betvalue != 1 {
		return customerror.ErrorOddValueInput
	}
	return handleResult(betEventID, betvalue)
}
