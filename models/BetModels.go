package models

import (
	"go_betting/customerror"
	"sync"
)

type Bet struct {
	betid           int
	bettorAccountId int
	bettingeventid  int
	value0          float32
	value1          float32
}

type BetType struct {
	sync.Mutex
	list map[int]*Bet
}

var Bets BetType = BetType{list: make(map[int]*Bet)}

//Create New Bet
func NewBet(betid int, bettorAccountId int, bettingeventid int) *Bet {
	return &Bet{
		betid:           betid,
		bettorAccountId: bettorAccountId,
		bettingeventid:  bettingeventid,
		value0:          0,
		value1:          0,
	}
}

//Add NewBet To AllBets
func (B *BetType) NewBet(bettorAccountId int, bettingeventid int) int {

	newBet := NewBet(len(B.list)+1, bettorAccountId, bettingeventid)
	B.Lock()
	B.list[int(newBet.betid)] = newBet
	B.Unlock()

	return newBet.betid
}

// Private placeBet Function
func (B *Bet) placeBet(betID int, betEventID int, bettorAccountId int, betvalue byte, valuebetAmount float32) error {
	// is bet valid
	// add bet to struct
	// recalculate sums
	// return nil
	if !isValidBet(betEventID, betvalue, valuebetAmount) {
		return customerror.ErrorValidatePlaceBet
	}
	if betvalue == 0 {
		BetEvents.list[betEventID].sumValue0 += valuebetAmount
		B.value0 += valuebetAmount
	} else if betvalue == 1 {

		BetEvents.list[betEventID].sumValue1 += valuebetAmount
		B.value1 += valuebetAmount
	} else {
		return customerror.ErrorPlaceBet
	}
	return nil
}

//isValidBet is this bet acceptabel or not
func isValidBet(betEventID int, betvalue byte, valuebetAmount float32) bool {
	var tmp float32
	switch betvalue {
	case 0:
		tmp = BetEvents.list[betEventID].oddValue0 * (BetEvents.list[betEventID].sumValue0 + valuebetAmount)
		break
	case 1:
		tmp = BetEvents.list[betEventID].oddValue1 * (BetEvents.list[betEventID].sumValue1 + valuebetAmount)
		break
	default:
		return false
	}
	if tmp >= Liquiditypool.pool {
		return false
	} else {
		return true
	}
}

//PlaceBet Function
func (B *BetType) PlaceBettorBet(betID int, betvalue byte, valuebetAmount float32) error {
	// Check bettorAccountId Exists
	if betvalue != 0 && betvalue != 1 {
		return customerror.ErrorOddValueInput
	}
	if valuebetAmount < 0 {
		return customerror.ErrorAmountValueInput
	}
	for _, v := range B.list {
		if v.betid == betID {
			return v.placeBet(betID, v.bettingeventid, v.bettorAccountId, betvalue, valuebetAmount)
		}
	}
	return customerror.ErrorBettorAcountNotExist
}
