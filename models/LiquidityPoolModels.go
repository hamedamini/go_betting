package models

import (
	"go_betting/customerror"
)

type LiquidityPool struct {
	lpAccountId []int
	pool        float32
}

var Liquiditypool *LiquidityPool = &LiquidityPool{pool: 10000000.00}

//Add Provider Account
func (LP *LiquidityPool) addProvideLiquidity() int {
	NewLpAccountId := len(LP.lpAccountId) + 1
	LP.lpAccountId = append(LP.lpAccountId, NewLpAccountId)
	return NewLpAccountId
}

//Get Add Provider Account
func (LP *LiquidityPool) AddProvideLiquidity() int {
	return LP.addProvideLiquidity()
}

//Add Amount value  To Pool
func (LP *LiquidityPool) provideLiquidity(pool float32) {
	LP.pool += pool
}

//Providers Fund to the pool
func (LP *LiquidityPool) ProvideLiquidity(lpAccountId int, pool float32) error {
	//Check lpAccountId is provideLiquidity pool
	for i := range LP.lpAccountId {
		if LP.lpAccountId[i] == lpAccountId {
			LP.provideLiquidity(pool)
			return nil
		}
	}
	return customerror.ErrorProviderLiquidityAccountExists
}

//Return Balance available in the pool
func (LP *LiquidityPool) GetLpBalance() float32 {
	return LP.pool
}
