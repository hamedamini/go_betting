package main

import (
	"fmt"
	"go_betting/models"
)

func main() {
	// Start Scenario ...
	// Create BetEvent With (oddvalue0,oddvalue1)
	myBetEventID := models.BetEvents.NewBetEvent(1.55, 1.85)
	// Create Two Bettor In Bet System With NewBettor
	// This Func Only Create Bettor
	myBettor1AccountID := models.Bettors.NewBettor()
	myBettor2AccountID := models.Bettors.NewBettor()
	// Now Bettor Register In BetEvent
	// Note : Bettor Maybe Register  On Multiple BetEvent
	myBet1ID := models.Bets.NewBet(myBettor1AccountID, myBetEventID)
	myBet2ID := models.Bets.NewBet(myBettor2AccountID, myBetEventID)

	//Now Add Account To PoolProvider
	AccountIDProviderLiquidity := models.Liquiditypool.AddProvideLiquidity()

	//Add Fund To Pool With PoolProviders
	err := models.Liquiditypool.ProvideLiquidity(AccountIDProviderLiquidity, 2000.00)
	if err != nil {
		fmt.Println(err)
	}
	// Bettor Now Place Bet On BetEvent with Betid and BetEventId and AccountId also betValue and AmountBetValue
	err = models.Bets.PlaceBettorBet(myBet1ID, 0, 100.00)
	if err != nil {
		fmt.Println(err)
	}
	// Bettor Now Place Bet On BetEvent with Betid and BetEventId and AccountId also betValue and AmountBetValue
	err = models.Bets.PlaceBettorBet(myBet2ID, 1, 100.00)
	if err != nil {
		fmt.Println(err)
	}
	// Bettor Now Place Bet On BetEvent with Betid and BetEventId and AccountId also betValue and AmountBetValue
	err = models.Bets.PlaceBettorBet(myBet1ID, 0, 300.00)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Printf("Balance Pool Before CalculateBetResut %f\n", models.Liquiditypool.GetLpBalance())

	// Handle Result
	err = models.CalculateBetResult(myBetEventID, 0)
	if err != nil {
		fmt.Printf("Error %s", err)
	}

	// get Bettor Balance Pool
	ok, balance := models.Bettors.GetBalance(myBettor1AccountID)
	if ok {
		fmt.Printf("Balance Bettor %f\n", balance)
	}

	// get Bettor Balance Pool
	ok, balance = models.Bettors.GetBalance(myBettor2AccountID)
	if ok {
		fmt.Printf("Balance Bettor %f\n", balance)
	}

	fmt.Printf("Balance Pool After CalculateBetResut %f\n", models.Liquiditypool.GetLpBalance())
}
