package customerror

import (
	"errors"
)

var (
	ErrorAddBettor                      = errors.New("Add Bettor To BetEvent ")
	ErrorValidatePlaceBet               = errors.New("pool can not provide the winning amount to the bet")
	ErrorPlaceBet                       = errors.New("Place Bet To BetEvent")
	ErrorAddAmountBet                   = errors.New("Add Amount To Bettor Account")
	ErrorHandleResult                   = errors.New("Handle Result BetEvent")
	ErrorProviderLiquidityAccountExists = errors.New("Account Id  Exist In Pool Providers")
	ErrorBettorAcountNotExist           = errors.New("Bettor Account Not Exists")
	ErrorBetEventIDNotFound             = errors.New("Bet Event ID Not Found")
	ErrorOddValueInput                  = errors.New("OddValue Input not support")
	ErrorAmountValueInput               = errors.New("Amount Value Input greater zero")
)
